package com.example.demo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Lee
 * @date 2022/5/7 -21:25
 */
@Data
@TableName(value = "TB_USER")
public class UserQo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private List<Integer> age;
    private String email;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private List<Date> createTime;

}
