package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.User;
import com.example.demo.UserQo;

import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @date 2022/5/8 -11:36
 */
public interface UserService extends IService<User> {
    List<Map> test(UserQo qo);
}
