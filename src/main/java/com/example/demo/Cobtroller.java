package com.example.demo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Lee
 * @date 2022/5/7 -22:46
 */
@RestController
public class Cobtroller {

    @Autowired
    private UserMapper userMapper;

    private UserService userService;


    @GetMapping("/get")
    @ApiOperation(value = "获取一个", httpMethod ="Get",notes = "获取一个")
    public Map<String,Object> getOne(User user){
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.ge(null!=user.getCreateTime(),User::getCreateTime,user.getCreateTime()).
        le(null!=user.getEndTime(),User::getCreateTime,user.getEndTime());
        List<User> list = userMapper.selectList(wrapper);
        list.forEach(System.out::println);
        HashMap<String, Object> map = new HashMap<>();
        map.put("user",list);
        map.put("total",list.size());
        return map;
    }

    @PostMapping("/put")
    @ApiOperation(value = "修改", httpMethod ="Post",notes = "修改")
    public Map<String,Object> put(String ids){
//        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
////        wrapper.in("age", ids.split(",")).set("age", 99);
        User user;
        user = new User();
//        user.setCreateTime(LocalDate.now());
////        user.setEmail("test");
////        userService.update(user,wrapper);
////        int b = userMapper.update(user, wrapper);
////        HashMap<String, Object> map = new HashMap<>();
////        map.put("flag",b);
////        return map;

        return null;
    }


    @PostMapping("/post")
    @ApiOperation(value = "添加或者修改", httpMethod ="Post",notes = "添加或者修改")
    public Map<String,Object> post(@RequestBody User user){
        boolean b = userService.updateById(user);
        HashMap<String, Object> map = new HashMap<>();
        map.put("flag",b);
        return map;
    }

    @PostMapping("/test")
    @ApiOperation(value = "测试", httpMethod ="Post",notes = "测试")
    public Map<String,Object> test(@RequestBody UserQo qo){
        List<Map> maps=userService.test(qo);
        HashMap<String, Object> map = new HashMap<>();
        map.put("result",maps);
        return map;
    }
    @PostMapping("/test1")
    @ApiOperation(value = "测试1", httpMethod ="Post",notes = "测试")
    public Map<String,Object> test(@RequestBody List<Map> list){
        Map<String,Object> map3=new HashMap<>();
        map3.put("name","11");
        map3.put("age",20);
        list.add(map3);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map1 = list.get(i);
       /*     if (i==0){
                wrapper4.and(o->o.eq("name","lxh").eq("age",20));
            }else {*/
            wrapper.or(o->o.eq("name",map1.get("name")).eq("age",map1.get("age")));
//            }
        }
        List<User> list4 = userService.list(wrapper);
        HashMap<String, Object> map = new HashMap<>();
        map.put("result",list4);
        return map;
    }

 /*   @PostMapping("/putOne")
    @ApiOperation(value = "修改一个", httpMethod ="Post",notes = "修改一个")
    public Map<String,Object> putOne(User user){
        userService.update()
        return map;
    }*/

     @PostMapping("/testValidated")
    @ApiOperation(value = "测试校验", httpMethod ="Post",notes = "测试校验")
    public Map<String,Object> testValidated(@Validated User user){
//        userService.update()
        return null;
    }
}
