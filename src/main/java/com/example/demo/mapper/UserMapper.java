package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.User;
import com.example.demo.UserQo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @date 2022/5/7 -21:25
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<Map> test(UserQo qo);
}
