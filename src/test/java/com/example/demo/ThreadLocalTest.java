package com.example.demo;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class ThreadLocalTest {
    public static void main(String[] args){
        // 新开2个线程用于设置 & 获取 ThreadLoacl的值
//        MyRunnable runnable = new MyRunnable();
//        new Thread(runnable, "线程1").start();
//        new Thread(runnable, "线程2").start();
        ArrayList<User> users = new ArrayList<>();
        User user1 = new User();
        user1.setAge(10);
        User user2 = new User();
        user2.setAge(30);
        users.add(user1);
        users.add(user2);
        Integer[] a={1,2,3,4,5};
        List<Integer> list = Arrays.asList(a);
        Integer reduce = list.stream().reduce(7, BinaryOperator.maxBy(Integer::compareTo));
        System.out.println(reduce);
    }
    // 线程类
    public static class MyRunnable implements Runnable {
        // 创建ThreadLocal & 初始化
        private ThreadLocal<String> threadLocal = new ThreadLocal<String>(){
            @Override
            protected String initialValue() {
                return "初始化值";
            }
        };
        @Override
        public void run() {
            // 运行线程时，分别设置 & 获取 ThreadLoacl的值
            String name = Thread.currentThread().getName();
            threadLocal.set(name + "的threadLocal"); // 设置值 = 线程名
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + "：" + threadLocal.get());
        }
    }
}
