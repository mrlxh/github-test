package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootTest
class DemoApplicationTestsThread {

    class Ticket{
        private int number=30;
        private Lock lock=new ReentrantLock();
        private synchronized void sale(){
            if (number>0){
                System.out.println(Thread.currentThread().getName()+"卖出"+number--+"  "+number+"剩");
            }
        }
        private  void saleLock(){
            lock.lock();
            if (number>0){
                System.out.println(Thread.currentThread().getName()+"卖出"+number--+"  "+number+"剩");
            }
            lock.unlock();
        }
    }
    @Test
    public void testThread(){
        Ticket ticket = new Ticket();
        new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.saleLock();
            }
        },"AA").start();

        new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.saleLock();
            }
        },"bb").start();
    }



//    线程通信
    class Share{
        private  int number=0;
        private synchronized void add() throws InterruptedException {
            if (number!=0){
                wait();
            }else {
                number++;
                System.out.println(Thread.currentThread().getName()+"number="+number);
                notifyAll();
            }
        }

    private synchronized void jian() throws InterruptedException {
        if (number!=1){
            wait();
        }else {
            number--;
            System.out.println(Thread.currentThread().getName()+"number="+number);
            notifyAll();
        }
    }
    }
    @Test
    public void test(){
        Share share = new Share();
        new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {

                    share.add();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"AA").start();
        new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {

                    share.jian();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"bb").start();
    }
}
