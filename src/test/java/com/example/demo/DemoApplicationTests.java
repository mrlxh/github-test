package com.example.demo;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.UserService;
import com.example.demo.service.impl.UserServiceImpl2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootTest
class DemoApplicationTests {


    @Autowired
    private ApplicationContext applicationContext;
    @Test
    void contextLoads() {
        UserMapper userMapper = (UserMapper) applicationContext.getAutowireCapableBeanFactory().getBean("userMapper");
        userMapper.test(new UserQo());
    }

/*    @Autowired
    private UserMapper userMapper;*/

//    @Resource(name = "userServiceImpl2")
    @Autowired
    private UserServiceImpl2 userService;

    @Resource(name = "userServiceImpl2")
    private UserService userMapper;

    @Test
    public void testSelect() {
        ArrayList<Map> list = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        map.put("eare","区域");
        map.put("number","号码");
        HashMap<String, String> map2 = new HashMap<>();
        map2.put("eare","区域");
        map2.put("number","号码");
        list.add(map);
        list.add(map2);
        redisTemplate.opsForHash().put("fzdict","task", JSONUtil.toJsonStr(list));
//        redisTemplate.opsForValue().set("member:login:code:","1");
//        redisTemplate.opsForValue().set("member:login:code:","2");
    }


    @Autowired
    private StringRedisTemplate redisTemplate;
    @Test
    public void testRedis() {
        String s = testGet("tasktype", "ok");
        System.out.println(s);
    }
    public String testGet(String typeId,String dictId) {
        Object o = redisTemplate.opsForHash().get("fzdict", typeId);
        if (null!=o){
            Map map = JSONUtil.toBean(o.toString(), Map.class);
            Object value = map.getOrDefault(dictId,"");
            return value.toString();
        }
        return "";
    }

    @Test
    public void test(){
        List<Map<String,Object>> queryList=new ArrayList<>();
        Map<String,Object> map=new HashMap<>();
        map.put("name","JACK123456");
        map.put("age",30);
        Map<String,Object> mapT=new HashMap<>();
        map.put("name","TOM");
        map.put("age",99);
        queryList.add(map);
        queryList.add(mapT);

    }

    @Test
    public void test3(){
        List<User> list = userService.list(null);
        list.forEach(System.out::println);
    }

    @Test
    public void test2() {
//        A or（C and D)
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        String name;
//        wrapper.eq(StringUtils.isEmpty(name),"name",name);


        User user = new User();
        wrapper.eq("name","TOM")
                .or(o->o.eq("name","TOM").eq("age",20));
        List<User> list = userService.list(wrapper);

        //(AandB)or(CandD)
        QueryWrapper<User> wrapper2 = new QueryWrapper<>();
        wrapper2.and(o->o.eq("name","TOM").eq("age",20))
                .or(o->o.eq("name","TOM").eq("age",20));
        List<User> list2 = userService.list(wrapper2);

        //A or (B and ( C or D))
        QueryWrapper<User> wrapper3 = new QueryWrapper<>();
        wrapper3.eq("name","lxh")
                .or(o->o.eq("name","TOM").and(t->t.eq("name","lxh").or().eq("age",20)));
        List<User> list3 = userService.list(wrapper3);
        //

        List<Map<String,Object>> queryList=new ArrayList<>();
        Map<String,Object> map=new HashMap<>();
        map.put("name","JACK123456");
        map.put("age",30);
        Map<String,Object> mapT=new HashMap<>();
        map.put("name","TOM");
        map.put("age",99);
        queryList.add(map);
        queryList.add(mapT);
        QueryWrapper<User> wrapper4 = new QueryWrapper<>();

        for (int i = 0; i < queryList.size(); i++) {
            Map<String, Object> map1 = queryList.get(i);
       /*     if (i==0){
                wrapper4.and(o->o.eq("name","lxh").eq("age",20));
            }else {*/
                wrapper4.or(o->o.eq("name",map1.get("name")).eq("age",map1.get("age")));
//            }
        }
        List<User> list4 = userService.list(wrapper4);
    }

    class Ticket{
        private int number=30;
        private Lock lock=new ReentrantLock();
        private synchronized void sale(){
            if (number>0){
                System.out.println(Thread.currentThread().getName()+"卖出"+number--+"  "+number+"剩");
            }
        }
        private  void saleLock(){
            lock.lock();
            if (number>0){
                System.out.println(Thread.currentThread().getName()+"卖出"+number--+"  "+number+"剩");
            }
            lock.unlock();
        }
    }
    @Test
    public void testThread(){
        Ticket ticket = new Ticket();
        new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.saleLock();
            }
        },"AA").start();

        new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.saleLock();
            }
        },"bb").start();
    }

    @Test
    public void testaabb() {
        try {
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@//127.0.0.1:1521/orcl", "scott", "123456aB");
            User user = new User();
            //insert 1st row
            String inserting = "INSERT INTO test1(id,name11,name2) values(?,?,?)";
            System.out.println("insert " + inserting);//
            PreparedStatement ps = con.prepareStatement(inserting);
            ps.setBoolean(1, true);
            ps.setString(3, String.valueOf(123));
            ps.setString(3, null);
//            ps.setString(3, null);
//            ps.setString(3, user.getName());
            ps.executeUpdate();

            //insert 2nd row
//            inserting = "INSERT INTO test1(id,name1) values(?,?)";
//            System.out.println("insert " + inserting);//
//            ps = con.prepareStatement(inserting);
//            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Exception: " + ex);
        }
    }
    
    @Test
    public void testbbc(){
        
    }
}
